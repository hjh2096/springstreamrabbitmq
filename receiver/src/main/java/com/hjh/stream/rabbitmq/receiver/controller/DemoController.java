package com.hjh.stream.rabbitmq.receiver.controller;

import com.hjh.stream.rabbitmq.receiver.rabbitmq.help.IRabbitmqSink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class DemoController {


    @Autowired
    private IRabbitmqSink channel;

    @RequestMapping
    public String demo() {

        return "demo";
    }


    @RequestMapping("/send/{name}")
    public String send(@PathVariable String name) {

        return name;
    }
}
