package com.hjh.stream.rabbitmq.receiver.rabbitmq.receiver;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hjh.stream.rabbitmq.receiver.rabbitmq.help.IRabbitmqSink;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Slf4j
public class ReceiverDemo1 {

    @StreamListener(IRabbitmqSink.INPUT)
    public void input(Message<String> message) {


        if (message == null) {
            log.info("messge is null");
            return;
        }

        String payLoad = message.getPayload();
        if (StringUtils.isEmpty(payLoad)) {
            log.info("payLoad is null");
        }


        JSONObject json = JSON.parseObject(message.getPayload());

        System.out.println("一般监听收到：" + json.toJSONString());
    }

}
