package com.hjh.stream.rabbitmq.receiver.rabbitmq.help;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface IRabbitmqSink {


    String OUTPUT = "hjh";

    String INPUT = OUTPUT+"_"+Sink.INPUT;

//    @Output(OUTPUT)
//    MessageChannel output();

    @Input(INPUT)
    SubscribableChannel input();


}
