package com.hjh.stream.rabbitmq.receiver;

import com.hjh.stream.rabbitmq.receiver.rabbitmq.help.IRabbitmqSink;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableBinding({IRabbitmqSink.class})
public class ReceiverApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReceiverApplication.class, args);
	}
}
