package com.hjh.stream.rabbitmq.sender.controller;

import com.alibaba.fastjson.JSON;
import com.hjh.stream.rabbitmq.sender.dto.Person;
import com.hjh.stream.rabbitmq.sender.rabbitmq.help.IRabbitmqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class DemoController {


    @Autowired
    private IRabbitmqService channel;

    @RequestMapping
    public String demo() {

        return "demo";
    }


    @RequestMapping("/send/{name}")
    public String send(@PathVariable String name) {
        Person person = new Person();
        person.setName(name);
        String msgContent =  JSON.toJSONString(person);
        channel.output().send(MessageBuilder.withPayload(msgContent).build());
        return msgContent;
    }
}
