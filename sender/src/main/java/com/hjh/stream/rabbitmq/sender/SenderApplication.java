package com.hjh.stream.rabbitmq.sender;

import com.hjh.stream.rabbitmq.sender.rabbitmq.help.IRabbitmqService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableBinding({IRabbitmqService.class})
public class SenderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SenderApplication.class, args);
    }
}
